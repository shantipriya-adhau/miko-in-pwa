require('isomorphic-fetch')
import webpack from 'webpack'
export default {
  server: {
    port: 3000,
    host: '0.0.0.0',
  },
  publicRuntimeConfig: {
    appKey: 'miko_3',
    appVersion: Date.now(),
    mikoMaxHandle: process.env.MIKO_MAX_HANDLE,
    mikoHandle: process.env.MIKO_HANDLE,
    subscriptionProduct: process.env.SUBSCRIPTION_PRODUCT_SKU,
    baseURL: process.env.BASEURL,
    annualSubscriptionProduct: process.env.ANNUAL_SUBSCRIPTION_PRODUCT_SKU,
    middlewareUrl: process.env.NODE_ENV === 'production' ? `https://${process.env.BASEURL}/api/`: `http://${process.env.BASEURL}/api/`,
  },
  render: {
    bundleRenderer: {
      shouldPreload: (file, type) => {
        return ['font'].includes(type)
      },
    },
    static: {
      maxAge: 1000 * 60 * 60 * 24 * 7,
    },
  },
  privateRuntimeConfig: {
    storeURL: process.env.STORE_URL,
    storeToken: process.env.STORE_TOKEN,
    DefaultCountry: process.env.DEFAULT_COUNTRY,
    DefaultCurrency: process.env.DEFAULT_CURRENCY,
    CfSpaceId: process.env.CONTENTFUL_SPACE_ID,
    CfAccessToken: process.env.CONTENTFUL_ACCESS_TOKEN,
    CfProdEnv: process.env.CONTENTFUL_PROD_ENV,
    CfDevEnv: process.env.CONTENTFUL_DEV_ENV,
  },
  serverMiddleware: [
    {
      path: '/custom',
      handler: '~/server-middleware/custom-features.js',
    },
    {
      path: '/healthz',
      handler: '~/server-middleware/health-check.js',
      prefix: false,
    },
  ],
  head: {
    title: 'Miko 3 - Personal AI Robot For Kids | Miko Advanced STEM toys',
    meta: [
      {
        charset: 'utf-8',
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1',
      },
      {
        name: 'theme-color',
        content: '#EE6B6B',
      },
      {
        name: 'msapplication-navbutton-color',
        content: '#EE6B6B',
      },
      {
        name: 'apple-mobile-web-app-capable',
        content: 'yes',
      },
      {
        name: 'apple-mobile-web-app-status-bar-style',
        content: '#EE6B6B',
      },
      {
        name: 'og:image',
        hid: 'og:image',
        content: `https://${process.env.BASEURL}/homepage/miko-robo.webp`,
      },
      {
        name: 'og:image:secure_url',
        hid: 'og:image:secure_url',
        content: `https://${process.env.BASEURL}/homepage/miko-robo.webp`,
      },
      {
        name: 'twitter:image',
        hid: 'twitter:image',
        content: `https://${process.env.BASEURL}/homepage/miko-robo.webp`,
      },
      {
        name: 'twitter:image:alt',
        hid: 'twitter:image:alt',
        content: 'Miko 3',
      },
      {
        name: 'og:title',
        hid: 'og:title',
        content: 'Miko 3',
      },
      {
        name: 'twitter:title',
        hid: 'twitter:title',
        content: 'Miko 3',
      },
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico',
      },
      {
        rel: 'alternate',
        href:
          process.env.NODE_ENV === 'production'
            ? `https://${process.env.BASEURL}/`
            : `http://${process.env.BASEURL}/`,
        hreflang: 'en',
      },
      {
        rel: 'canonical',
        href:
          process.env.NODE_ENV === 'production'
            ? `https://${process.env.BASEURL}/`
            : `http://${process.env.BASEURL}/`,
      },
      {
        rel: 'preconnect',
        href: 'https://images.ctfassets.net/',
        crossorigin: 'anonymous',
      },
      // {
      //   rel: "preload",
      //   as: "image",
      //   href: "/homepage/miko-robo.webp",
      // },
      {
        rel: 'preload',
        as: 'image',
        // href: "/homepage/miko-robo-mobile.webp",
        href: '/homepage/christmas_miko.webp',
      },
      {
        rel: 'preload',
        as: 'image',
        // href: "/homepage/miko-robo-mobile.webp",
        href: 'http://images.ctfassets.net/g9ymljj7t64b/6XwlLLhkudSHD6VEmzFgya/4648ba3dd0e4c31013c9547de320fdf2/markus-spiske-5jeRBN4-ZLE-unsplash-removebg-preview_2.png?fm=webp',
      },
      {
        rel: 'preload',
        as: 'image',
        // href: "/homepage/miko-robo-mobile.webp",
        href: 'http://images.ctfassets.net/g9ymljj7t64b/5BkAFelvrBQEXwRu0a6Ren/039e2bc13b15bf8d415f8e8256144fd9/markus-spiske-5jeRBN4-ZLE-unsplash-removebg-preview_3.png?fm=webp',
      },
      {
        rel: 'preload',
        as: 'font',
        crossorigin: 'crossorigin',
        type: 'font/woff2',
        href: '/fonts/DegularDisplay-Medium.woff2',
      },
      {
        rel: 'preload',
        as: 'font',
        crossorigin: 'crossorigin',
        type: 'font/woff2',
        href: '/fonts/DegularDisplay-Bold.woff2',
      },
      {
        rel: 'preload',
        as: 'font',
        crossorigin: 'crossorigin',
        type: 'font/woff2',
        href: '/fonts/DegularDisplay-Black.woff2',
      },
      {
        rel: 'preload',
        as: 'font',
        crossorigin: 'crossorigin',
        type: 'font/woff2',
        href: '/fonts/DegularDisplay-Regular.woff2',
      },
    ],
  },
  css: ['~/assets/css/styles'],
  loading: {
    color: '#EE6B6B',
  },
  plugins: ['~/plugins/scrollToTop.client.js'],
  buildModules: [
    // to core
    [
      '@nuxtjs/pwa',
      {
        workbox: {
          enabled: true,
        },
      },
    ],
    '@nuxt/typescript-build',
    '@nuxtjs/style-resources',
    '@nuxtjs/device',
    [
      '@vue-storefront/nuxt',
      {
        useRawSource: {
          dev: ['@vue-storefront/shopify', '@vue-storefront/core'],
          prod: ['@vue-storefront/shopify', '@vue-storefront/core'],
        },
      },
    ],
    ['@vue-storefront/nuxt-theme'],
    [
      '@vue-storefront/shopify/nuxt',
      {
        i18n: {
          useNuxtI18nConfig: true,
        },
      },
    ],
  ],
  modules: [
    'nuxt-i18n',
    'cookie-universal-nuxt',
    'vue-scrollto/nuxt',
    '@vue-storefront/middleware/nuxt',
    '~/modules/integrations.js',
    '@nuxtjs/gtm',
    '@nuxtjs/device',
  ],
  gtm: {
    id: process.env.GTM_ID,
    enabled: true,
    pageTracking: false,
    pageViewEventName: 'PageView',
    autoInit: false,
    respectDoNotTrack: true,
  },
  i18n: {
    baseUrl: `//${process.env.BASEURL}`,
    currency: 'INR',
    country: 'IN',
    countries: [
      {
        name: 'IN',
        label: 'India',
      },
    ],
    currencies: [
      {
        name: 'INR',
        label: 'Indian rupee',
      },
    ],
    locales: [
      {
        code: 'in',
        label: 'India',
        file: 'en.js',
        iso: 'en-IN',
      },
    ],
    defaultLocale: 'in',
    seo: false,
    langDir: 'lang/',
    vueI18n: {
      numberFormats: {
        in: {
          currency: {
            style: 'currency',
            currency: 'INR',
            currencyDisplay: 'symbol',
            minimumFractionDigits: 0,
            autoDecimalDigits: false
          },
          decimal: {
            style: 'decimal',
            currency: 'INR',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
            autoDecimalDigits: false
          },
          percent: {
            style: 'percent',
            useGrouping: false,
          },
        },
      },
    },
    detectBrowserLanguage: {
      cookieKey: 'vsf-locale',
    },
  },
  styleResources: {
    scss: [
      require.resolve('@storefront-ui/shared/styles/_helpers.scss', {
        paths: [process.cwd()],
      }),
    ],
  },
  build: {
    transpile: ['vee-validate/dist/rules'],
    plugins: [
      new webpack.DefinePlugin({
        'process.VERSION': JSON.stringify({
          // eslint-disable-next-line global-require
          version: require('./package.json').version,
          lastCommit: process.env.LAST_COMMIT || '',
        }),
      }),
    ],
    extractCSS: {
      ignoreOrder: true,
    },
  },
  router: {
    extendRoutes(routes, resolve) {
      routes.push(
        {
          name: 'singleProduct',
          path: '/store/:slug/',
          component: resolve(__dirname, './pages/Product.vue'),
          chunkName: 'pages/Product',
        },
        {
          name: 'MikoMax-Home',
          path: '/store/mikomax',
          component: resolve(__dirname, './pages/MikoMax.vue'),
          chunkName: 'pages/mikomax',
        },
        {
          name: 'Trade-In',
          path: '/store/tradeIn',
          component: resolve(__dirname, './pages/TradeIn.vue'),
          chunkName: 'pages/tradeIn',
        },
        {
          name: 'homelead',
          path: '/store/lead',
          component: resolve(__dirname, './pages/HomeLead.vue'),
          chunkName: 'pages/lead',
        }
      )
    },
    scrollBehavior: async (to, from, savedPosition) => {
      if (savedPosition) {
        return savedPosition
      }

      const smoothscrollPolyfillScript =
        '//unpkg.com/smoothscroll-polyfill@0.4.4/dist/smoothscroll.min.js'

      if (
        process.client &&
        !('scrollBehavior' in document.documentElement.style)
      ) {
        if (
          document.querySelectorAll(
            `script[src="${smoothscrollPolyfillScript}"]`
          ).length === 0
        ) {
          const ele = document.createElement('script')
          ele.src = smoothscrollPolyfillScript
          ele.defer = true
          document.body.appendChild(ele)
        }
      }

      const findEl = async (hash, x) => {
        return (
          document.querySelector(hash) ||
          new Promise((resolve, reject) => {
            if (x > 50) {
              return resolve()
            }
            setTimeout(() => {
              resolve(findEl(hash, ++x || 1))
            }, 100)
          })
        )
      }

      if (to.hash) {
        setTimeout(async () => {
          let el = await findEl(to.hash)
          return window.scrollTo({
            top: el.offsetTop,
            behavior: 'smooth',
          })
        }, 2000)
      }

      return {
        x: 0,
        y: 0,
      }
    },
  },
  pwa: {
    manifest: {
      name: 'Miko 3',
      lang: 'en',
      short_name: 'Miko 3',
      startUrl: '/',
      display: 'browser',
      background_color: '#FFFFFF',
      theme_color: '#EE6B6B',
      description:
        'Miko 3 is an advanced AI robot toy made to engage, educate and entertain kids. Miko 3 robot is a programmable and voice activated STEM toy to make kids smarter',
      icons: [
        {
          src: '/icons/android-icon-48x48.png',
          sizes: '48x48',
          type: 'image/png',
        },
        {
          src: '/icons/android-icon-72x72.png',
          sizes: '72x72',
          type: 'image/png',
        },
        {
          src: '/icons/android-icon-96x96.png',
          sizes: '96x96',
          type: 'image/png',
        },
        {
          src: '/icons/android-icon-144x144.png',
          sizes: '144x144',
          type: 'image/png',
        },
        {
          src: '/icons/android-icon-168x168.png',
          sizes: '168x168',
          type: 'image/png',
        },
        {
          src: '/icons/android-icon-192x192.png',
          sizes: '192x192',
          type: 'image/png',
        },
        {
          src: '/icons/android-icon-512x512.png',
          sizes: '512x512',
          type: 'image/png',
        },
      ],
    },
    meta: {
      name: 'Miko 3',
      author: 'Miko 3',
      lang: 'en',
      short_name: 'miko3',
      start_url: '/',
      display: 'standalone',
      background_color: '#FFFFFF',
      theme_color: '#EE6B6B',
      description:
        'Miko 3 is an advanced AI robot toy made to engage, educate and entertain kids. Miko 3 robot is a programmable and voice activated STEM toy to make kids smarter',
      ogHost: process.env.BASEURL,
    },
    icon: {
      iconSrc: 'src/static/android-icon-512x512.png',
    },
    workbox: {
      offlineStrategy: 'NetworkFirst',
      runtimeCaching: [
        {
          // Match any request that ends with .png, .jpg, .jpeg or .svg .gif.
          urlPattern: /\.(?:png|jpg|jpeg|svg|woff|woff2|webp|json|gif)$/,
          // Apply a cache-first strategy.
          handler: 'CacheFirst',
          options: {
            // Use a custom cache name.
            cacheName: 'mikoAssets',

            // Only cache 100 assets.
            expiration: {
              maxEntries: 100,
              maxAgeSeconds: 7200,
            },
          },
        },
        {
          urlPattern: `//${process.env.BASEURL}/.*`,
          handler: 'StaleWhileRevalidate',
          method: 'GET',
          strategyOptions: {
            cacheableResponse: {
              statuses: [0, 200],
            },
          },
        },
        {
          urlPattern: `//assets.ctfassets.net/.*`,
          handler: 'CacheFirst',
        },
        {
          urlPattern: `//images.ctfassets.net/.*`,
          handler: 'CacheFirst',
        },
      ],
      preCaching: [
        '/homepage/as-seen-on-1.png',
        '/homepage/as-seen-on-2.png',
        '/homepage/as-seen-on-3.png',
        '/homepage/as-seen-on-4.png',
        '/homepage/as-seen-on-5.png',
        '/homepage/as-seen-on-6.png',
        '/homepage/christmas_miko.webp',
        '/fonts/DegularDisplay-Black.woff2',
        '/fonts/DegularDisplay-Regular.woff2',
        '/fonts/DegularDisplay-Bold.woff2',
        '/fonts/DegularDisplay-Medium.woff2',
      ],
    },
  },
}
