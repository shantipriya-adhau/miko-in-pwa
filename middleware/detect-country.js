/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
// eslint-disable-next-line func-names
export default context => {
  const available_i18n = context.i18n.localeCodes

  if (context.req !== undefined) {
    const splitedURL = context.req.url.split('/')
    const countryCode =
      context.app.$device.isCrawler && available_i18n.includes(splitedURL[1])
        ? splitedURL[1]
        : context.req.headers['cf-ipcountry']
        ? context.req.headers['cf-ipcountry'].toLowerCase()
        : 'in'

    //set locale cookie
    context.$cookies.set('visitor_country_code', countryCode, {
      maxAge: 60 * 60 * 24 * 30,
      path: '/',
    })

    if (countryCode !== 'in' || countryCode === '') {
      context.redirect('https://miko.ai')
    }
  }
}
