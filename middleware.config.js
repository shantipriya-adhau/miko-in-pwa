module.exports = {
  integrations: {
    shopify: {
      location: '@vue-storefront/shopify-api/server',
      configuration: {
        api: {
          domain: process.env.STORE_URL,
          storefrontAccessToken: process.env.STORE_TOKEN,
          apiVersion: "2022-01"
        },
        currency: process.env.DEFAULT_CURRENCY,
        country: process.env.DEFAULT_COUNTRY
      }
    },
  }
};
