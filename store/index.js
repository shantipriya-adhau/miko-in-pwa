export const state = () => ({
  commonElements: null,
  homeElements: null,
  userInfo: null,
  cartTotal: 0,
  chosenSubscriptionVariant: '',
  cartItems: null,
  allProducts: [],
  isExtendEnabled: false,
  isNokEnabled: false,
  isTopBarEnabled: false,
  bundleProduct: {},
  currencySymbols: { in: '₹' },
})

export const mutations = {
  SET_COMMONELEMENTS(state, message) {
    state.commonElements = message
  },
  SET_HOMEELEMENTS(state, message) {
    state.homeElements = message
  },
  SET_CARTTOTAL(state, totalItems) {
    state.cartTotal = totalItems
  },
  SET_CART_ITEMS(state, cartItems) {
    state.cartItems = cartItems
  },
  SET_DEFAULT_VARIANT(state, chosenSubscriptionVariant) {
    state.chosenSubscriptionVariant = chosenSubscriptionVariant
  },
  SET_USER(state, userInfo) {
    state.userInfo = userInfo
  },
  SET_ALLPRODUCTS(state, allProducts) {
    state.allProducts = allProducts
  },
  SET_EXTEND_STATUS(state, status) {
    state.isExtendEnabled = status
  },
  SET_NOK_STATUS(state, status) {
    state.isNokEnabled = status
  },
  SET_TOPBAR_STATUS(state, status) {
    state.isTopBarEnabled = status
  },
  SET_BUNDLE_PRODUCT_INFO(state, info) {
    state.bundleProduct = info
  },
}

export const actions = {
  async nuxtServerInit({ commit }, context) {
    const curLocale =
      context.app.i18n.locale && context.app.i18n.locale === 'es'
        ? context.app.i18n.locale + '-' + context.app.i18n.locale.toUpperCase()
        : 'en-US'

    await context.$CfClient
      .getEntries({
        content_type: 'componentCommonElements',
        include: 9,
        locale: curLocale,
        select: ['fields.commonElements'],
      })
      .then(entries => {
        commit('SET_COMMONELEMENTS', entries)
      })
      .catch(error => {
        console.error(error)
        commit('SET_COMMONELEMENTS', { error: 'something went wrong!' })
      })

    await context.$CfClient
      .getEntries({
        content_type: 'pwaLandingPageStorefrontHome',
        locale: curLocale,
        select: [
          'fields.heroBanner',
          'fields.introSection',
          'fields.tesimonials',
          'fields.customerTestimonials',
          'fields.sayItLoudProud',
          'fields.sectionSimplySecure',
          'fields.sectionMikoMax',
          'fields.getToKnowMikoSection',
          'fields.gameForAnythingSection',
          'fields.sectionWhosTalkingAboutMikoTitle',
          'fields.sectionCustomerTestimonialsTitle',
          'fields.preOrderBanner',
        ],
      })
      .then(entries => {
        commit('SET_HOMEELEMENTS', entries)
      })
      .catch(error => {
        console.error(error)
        commit('SET_HOMEELEMENTS', { error: 'something went wrong!' })
      })
  },
}
