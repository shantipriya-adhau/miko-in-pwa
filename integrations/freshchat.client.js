import { afterLoad } from '~/helpers/afterloadforci';
export default ({ $device}) => {
    afterLoad(() => {
        if (!$device.isCrawler) {
            const initFreshChat = () => {
                window.fcWidget.init({
                    token: "d4c25f00-4311-4045-82d4-90dd0c79f825",
                    host: "https://wchat.freshchat.com"
                });
            }
            const initialize = (i, t) => {
                var e;
                i.getElementById(t) ? initFreshChat() : ((e = i.createElement("script")).id = t, e.defer = !0, e.src = "https://wchat.freshchat.com/js/widget.js", e.onload = initFreshChat, i.head.appendChild(e))
            }
            const initiateCall = () => {
                initialize(document, "Freshdesk Messaging-js-sdk")
            }
            // initiate freshchat
            initiateCall();
        }
  });
};