import { afterLoad } from '~/helpers/afterloadforci';
export default () => {
    afterLoad(() => {
      // Storing Discount utm param in session storage if available 
      const queryString = window.location.search;
      const urlParams = new URLSearchParams(queryString);
      const discount_code = urlParams.get('discount')
      if(discount_code) {
        sessionStorage.setItem('discount_code', discount_code)
      }
      // Storing Discount utm param in session storage if available
  });
};