import { afterLoad } from '~/helpers/afterloadforci';
export default ({ $gtm, $device, $cookies, app}) => {
  afterLoad(() => {
    if (!$device.isCrawler) {
      $gtm.init();
        // Trigger pageview for cart open
        $gtm.push({event: 'ViewContent', action: 'PageView', value: app.context.route.path, deviceid: $cookies.get('deviceUUID')});
    }
  });
};
