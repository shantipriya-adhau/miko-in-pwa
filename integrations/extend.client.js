import { afterLoad } from '~/helpers/afterloadforci';
export default ({ $device, store }) => {
    afterLoad(() => {
         function async(u, c) {
            var d = document, t = 'script',
                o = d.createElement(t),
                s = d.getElementsByTagName(t)[0];
            o.src = '//' + u;
            if (c) { o.addEventListener('load', (e) => { c(null, e); }, false); }
            s.parentNode.insertBefore(o, s);
        }
        if (!$device.isCrawler) {
            if (!store.state.isExtendEnabled) {
                return;
            }
            async('//sdk.helloextend.com/extend-sdk-client/v1/extend-sdk-client.min.js', () => {
                if (Extend) {
                    Extend.config({ storeId: 'de4c2b9f-935a-4dc3-8d81-8fb744780838', environment: 'production' });
                }
            });
        }
  });
};