export const getCmsData = (object) => {
  let cmsItem = {};
  object.map( item => {
    cmsItem = item.fields; 
  });
  return cmsItem;
}
export const getMetaTitle = (object) => {
  let title = '';
  object.map( item => {
    title = item.fields.seoTitle;; 
  });
  return title;
};
export const getMetaDescription = (object) => {
  let desc = '';
  object.map( item => {
    desc = item.fields.seoDescription;
  });
  return desc;
};
export const getContentfulAssetById = (ContentfulproductAssets, AssetId) => {
    if (typeof AssetId !== 'string') return '';
    const assetData = ContentfulproductAssets.find(asset => asset.sys.id === AssetId);
    return assetData.fields.file;
  };
  // Get entries by its ID
  export const getContentfulEntryById = (ContentfulproductEntries, EntryId) => {
    if (typeof EntryId !== 'string') return '';
    const entryData = ContentfulproductEntries.find(entry => entry.sys.id === EntryId);
    return entryData.fields;
  };

  const getSectionDataByKey = (sectionDataArray, sectionId) => {
    for (const [key, value] of Object.entries(sectionDataArray.fields)) {
      if (key === sectionId) {
        return value;
      }
    }
  };
  
  export const getContentsBySectionKey = (ContentfulItems, sectionKey) => {
    if (ContentfulItems && ContentfulItems.items) {
      const sectionId = sectionKey.sectionId;
      let SectionData;
      (ContentfulItems.items).forEach(sectionDataArray => {
        SectionData =  getSectionDataByKey(sectionDataArray, sectionId);
      });
      return SectionData
    }
  };
  export const getFilteredContents = (elements, sectionKey) => {
    if (elements && elements.length) {
      const sectionId = sectionKey.contentTypeId;
      return elements.find(data => {
        return data.sys.contentType.sys.id === sectionId
      });
    }
  };
  export const getAllFilteredContents = (elements, sectionKey) => {
    if (elements && elements.length) {
      const sectionId = sectionKey.contentTypeId;
      return elements.filter(data => {
        return data.sys.contentType.sys.id === sectionId
      });
      
    }
  };
export const getFileUrl = (content, device = '') => {
    if (content.hasOwnProperty('file')) {
      const fileType = checkFileType(content, 'image')
      return device && (device.isMacOS || device.isIos) ? `${content.file.url}` : fileType ? `${content.file.url}?fm=webp` : content.file.url;
    }
  };
  export const checkFileType = (fileObj, contentType) => {
    if (fileObj.file.hasOwnProperty('contentType')) {
      return (fileObj.file.contentType).includes(contentType)
    }
  };
  export const getImageDimentions = (content) => {
    if (content && content.hasOwnProperty('file')) {
      return content.file.details.image
    }
  };
  export const getContentValue = (caption) => {
    if (caption && caption.hasOwnProperty('content')) {
      let secTitle;
      (caption.content).map(c => {
        (c.content).map(val => {
          secTitle = val.value;
        })
      })
      return secTitle;
    }
  }
  export const getContentByFieldsTitle = (content, fieldTitle) => {
    if (content && content && content.length > 0) {
      return content.find(data => {
        if (data.fields.title !== 'undefined' && data.fields.title === fieldTitle) {
          return data;
        }
      })
    }
  }
  export const getMenuByPosition = (menu, position) => {
    if (menu && menu.length > 0) {
      const menuObj = menu.find(data => {
        if (data.fields.menuPosition !== 'undefined' && data.fields.menuPosition === position) {
          return data;
        }
      })
      return menuObj && menuObj.hasOwnProperty('fields') ? menuObj.fields.menuItems : {}
    }
  }

export const getEncryptedVariantID = (variationIDPlain) => {
    const buff = Buffer.from(`gid://shopify/ProductVariant/${variationIDPlain}`);
    return buff.toString('base64');
}
export const getVariantByID = (productInfo, DefaultVariantId) => {
  for(let i=0; i < (productInfo).length; i++) {
    for(let j=0; j < (productInfo[i].variants).length; j++) {
      if(productInfo[i].variants[j].id == DefaultVariantId) {
        return productInfo[i].variants[j];
      }
    }
  }
}