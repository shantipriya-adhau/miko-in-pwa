const bodyParser = require('body-parser')
const app = require('express')()
const axios = require('axios')
module.exports = {
  path: '/',
  handler: app,
}
app.use(bodyParser.json())

app.get('/cart.js', (req, res) => {
  var options = {
    method: 'GET',
    url: `https://${process.env.STORE_URL}` + req.originalUrl,
  }
  axios
    .request(options)
    .then(response => {
      res.status(200).send(response.data)
    })
    .catch(error => {
      console.error(error)
    })
})
/**
 * Redirection for Shopify
 */
app.post('/account/login', (req, res) => {
  res.json({
    success: 'test',
  })
})

/**
 * Redirection for Shopify
 */
app.get('/account/*', (req, res) => {
  res.redirect(`https://${process.env.STORE_URL}` + req.originalUrl)
})

app.get('/12242124862/*', (req, res) => {
  res.redirect(`https://${process.env.STORE_URL}` + req.originalUrl)
})

app.get('/cart', (req, res) => {
  res.json({
    status: '200',
    message: 'CART:: You did it!',
    dir: __dirname,
  })
})
app.post('/product-variant-price', (req, res) => {
  var options = {
    method: 'POST',
    url: `https://${process.env.STORE_URL}/api/2021-07/graphql.json`,
    params: {
      '': '',
    },
    headers: {
      'X-Shopify-Storefront-Access-Token': '3eb5245b0fd3b115a449e83f15daf83d',
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    data: JSON.stringify({
      query:
        'query  @inContext(country: ' +
        req.body.country_code +
        ') {\n  node(id: "' +
        req.body.variant_id +
        '") {\n    ... on ProductVariant {\n      priceV2 {\n          amount\n          currencyCode\n      }\n    }\n  }\n}',
    }),
  }

  axios
    .request(options)
    .then(response => {
      res.json(response.data)
    })
    .catch(error => {
      res.json({
        error: true,
        stack: error,
      })
    })
})

/**
 * Generate Sitemap data
 */

app.get('/:countryiso?/robots.txt', (req, res) => {
  const locale = req.params.countryiso ? req.params.countryiso + '/' : ''
  res.type('text/plain')
  var responseXML = `User-agent: Googlebot \nDisallow:  \n\nUser-agent: Googlebot-image \nDisallow: \n\nUser-agent: * \nDisallow:  \n\nSitemap: https://${process.env.STORE_PRIMARY_URL}/${locale}sitemap.xml`
  res.status(200).send(responseXML)
})

app.get('/:countryiso?/sitemap.xml', (req, res) => {
  const locale = req.params.countryiso ? req.params.countryiso + '/' : ''

  const dynamic = [
    'store/miko3',
    'store/mikomax',
    'store/tradeIn',
    'store/lead',
    'track-order',
    'about-us',
    'faq',
    'contact-us',
    'terms-and-conditions',
    'privacy-policy',
    'shipping-policy',
  ]

  var responseXML = ''
  responseXML +=
    '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xmlns:mobile="http://www.google.com/schemas/sitemap-mobile/1.0" xmlns:news="http://www.google.com/schemas/sitemap-news/0.9" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1" xmlns:xhtml="http://www.w3.org/1999/xhtml">'

  dynamic.forEach((pageUrl, index) => {
    responseXML += '<url>'
    responseXML += '<loc>https://miko.ai/' + locale + pageUrl + '</loc>'
    responseXML += '</url>'
  })

  responseXML += '</urlset>'

  res.status(200).set('Content-Type', 'text/xml').send(responseXML)
})

app.get('/:countryiso?/catalog_rss.xml', (req, res) => {
  const locale = req.params.countryiso ? req.params.countryiso : 'en'
  const currencySymbols = {
    in: 'INR',
    ca: 'CAD',
    it: 'GBP',
    es: 'GBP',
    de: 'EUR',
    fr: 'EUR',
    gb: 'GBP',
    en: 'USD',
  }
  var options = {
    method: 'GET',
    url: `https://7601ca579ffd169dac4e4fd132d9779e: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@${
      process.env.STORE_URL_SECONDARY
    }/admin/api/2022-01/products/6628829003838.json?presentment_currencies=${
      currencySymbols.hasOwnProperty(locale) ? currencySymbols[locale] : 'USD'
    }`,
    headers: { 'X-Shopify-Api-Features': 'include-presentment-prices' },
  }

  var optionsMax = {
    method: 'GET',
    url: `https://7601ca579ffd169dac4e4fd132d9779e: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@${
      process.env.STORE_URL_SECONDARY
    }/admin/api/2022-01/products/6617542721598.json?presentment_currencies=${
      currencySymbols.hasOwnProperty(locale) ? currencySymbols[locale] : 'USD'
    }`,
    headers: { 'X-Shopify-Api-Features': 'include-presentment-prices' },
  }

  axios
    .all([axios.request(options), axios.request(optionsMax)])
    .then(
      axios.spread((response, responseMax) => {
        // output of req.
        let lang =
          req.params.countryiso !== undefined
            ? '/' + req.params.countryiso + '/'
            : '/'
        var responseXML = ''
        responseXML +=
          '<?xml version="1.0" encoding="UTF-8"?><rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">'
        responseXML += '<channel>'
        responseXML +=
          '<title>Miko 3 - Personal AI Robot For Kids | Miko Advanced STEM toys</title>'
        responseXML +=
          `<link>https://${process.env.STORE_PRIMARY_URL}` + lang + '</link>'
        responseXML +=
          '<description>Miko 3 is an advanced AI robot toy made to engage, educate and entertain kids. Miko 3 robot is a programmable and voice activated STEM toy to make kids smarter.</description>'
        // Miko 3
        for (let [i, variant] of response.data.product.variants.entries()) {
          responseXML += '<item>'
          responseXML +=
            '<g:id>shopify_US_' +
            response.data.product.id +
            '_' +
            variant.id +
            '</g:id>'
          responseXML +=
            '<g:title><![CDATA[' + response.data.product.title + ']]></g:title>'
          responseXML +=
            '<g:description><![CDATA[' +
            response.data.product.body_html.replace(/<\/?[^>]+>/gi, ' ') +
            ']]></g:description>'
          responseXML +=
            `<g:link>https://${process.env.STORE_PRIMARY_URL}` +
            lang +
            'store/miko3?Color=' +
            encodeURIComponent(variant.title) +
            '</g:link>'
          responseXML +=
            '<g:image_link>' +
            response.data.product.images
              .find(img => img.id === variant.image_id)
              .src.split('?')[0] +
            '</g:image_link>'
          responseXML += '<g:condition>new</g:condition>'
          responseXML += '<g:availability>' + 'in stock' + '</g:availability>'
          if (variant.presentment_prices[0].compare_at_price.amount > 0) {
            responseXML +=
              '<g:price>' +
              variant.presentment_prices[0].compare_at_price.amount +
              ' ' +
              currencySymbols[locale] +
              '</g:price>' // NEED TO CHANGE CURRENCY AND PRICE AS PER LOCALE
            responseXML +=
              '<g:sale_price>' +
              variant.presentment_prices[0].price.amount +
              ' ' +
              currencySymbols[locale] +
              '</g:sale_price>' // NEED TO CHANGE CURRENCY AND PRICE AS PER LOCALE
          } else {
            responseXML +=
              '<g:price>' +
              variant.presentment_prices[0].price.amount +
              ' ' +
              currencySymbols[locale] +
              '</g:price>' // NEED TO CHANGE CURRENCY AND PRICE AS PER LOCALE
          }
          if (variant.title === 'Martian Red') {
            responseXML += '<g:gtin>8908014554309</g:gtin>'
          } else if (variant.title === 'Pixie Blue') {
            responseXML += '<g:gtin>8908014554316</g:gtin>'
          }
          responseXML += '<g:brand>Miko</g:brand>'
          responseXML += '<g:color>' + variant.title + '</g:color>'
          responseXML +=
            '<g:item_group_id>' +
            response.data.product.id +
            '</g:item_group_id>'
          responseXML += '</item>'
        }

        // Miko Max
        for (let [i, variant] of responseMax.data.product.variants.entries()) {
          responseXML += '<item>'
          responseXML +=
            '<g:id>shopify_US_' +
            responseMax.data.product.id +
            '_' +
            variant.id +
            '</g:id>'
          responseXML +=
            '<g:title><![CDATA[' +
            responseMax.data.product.title +
            ']]></g:title>'
          responseXML +=
            '<g:description><![CDATA[' +
            responseMax.data.product.body_html.replace(/<\/?[^>]+>/gi, ' ') +
            ']]></g:description>'
          responseXML +=
            `<g:link>https://${process.env.STORE_PRIMARY_URL}` +
            lang +
            'store/mikomax</g:link>'
          responseXML +=
            '<g:image_link>' +
            responseMax.data.product.images
              .find(img => img.id === variant.image_id)
              .src.split('?')[0] +
            '</g:image_link>'
          responseXML += '<g:condition>new</g:condition>'
          responseXML += '<g:availability>' + 'in stock' + '</g:availability>'
          if (
            variant.presentment_prices[0].compare_at_price &&
            variant.presentment_prices[0].compare_at_price.amount > 0
          ) {
            responseXML +=
              '<g:price>' +
              variant.presentment_prices[0].compare_at_price.amount +
              ' ' +
              currencySymbols[locale] +
              '</g:price>' // NEED TO CHANGE CURRENCY AND PRICE AS PER LOCALE
            responseXML +=
              '<g:sale_price>' +
              variant.presentment_prices[0].price.amount +
              ' ' +
              currencySymbols[locale] +
              '</g:sale_price>' // NEED TO CHANGE CURRENCY AND PRICE AS PER LOCALE
          } else {
            responseXML +=
              '<g:price>' +
              variant.presentment_prices[0].price.amount +
              ' ' +
              currencySymbols[locale] +
              '</g:price>' // NEED TO CHANGE CURRENCY AND PRICE AS PER LOCALE
          }
          if (variant.title === 'One Time - 1 Year') {
            responseXML += '<g:gtin>8908014554354</g:gtin>'
          } else if (variant.title === 'One Time - 3 Months') {
            responseXML += '<g:gtin>8908014554330</g:gtin>'
          }
          responseXML += '<g:brand>Miko</g:brand>'
          responseXML +=
            '<g:item_group_id>' +
            responseMax.data.product.id +
            '</g:item_group_id>'
          responseXML += '</item>'
        }

        responseXML += '</channel>'
        responseXML += '</rss>'

        res.status(200).set('Content-Type', 'text/xml').send(responseXML)
      })
    )
    .catch(error => {
      console.error(error)
      let lang =
        req.params.countryiso !== 'us' ? '/' + req.params.countryiso + '/' : '/'
      var responseXML = ''
      responseXML +=
        '<?xml version="1.0" encoding="UTF-8"?><rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">'
      responseXML += '<channel>'
      responseXML +=
        '<title>Miko 3 - Personal AI Robot For Kids | Miko Advanced STEM toys</title>'
      responseXML +=
        `<link>https://${process.env.STORE_PRIMARY_URL}` + lang + '</link>'
      responseXML +=
        '<description>Miko 3 is an advanced AI robot toy made to engage, educate and entertain kids. Miko 3 robot is a programmable and voice activated STEM toy to make kids smarter.</description>'
      responseXML += '</channel>'
      responseXML += '</rss>'

      res.status(200).set('Content-Type', 'text/xml').send(responseXML)
    })

  //res.status(200).send('Thank you!');
})
app.get('/tradeinform/:name/:email/:merges', (req, res) => {
  // MailChimp API integration
  // https://mailchimp.com/developer/marketing/api/list-members/add-member-to-list/

  const API_KEY = 'a11273356df50d0f17aa282a790f881a-us20',
    API_DC = 'us20',
    LISTID = '587a079f80'

  var options = {
    method: 'POST',
    url:
      'https://' +
      API_DC +
      '.api.mailchimp.com/3.0/lists/' +
      LISTID +
      '/members',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + API_KEY,
    },
    data: {
      merge_fields: {
        MERGE5: req.params.merges,
        NAME: req.params.name,
      },
      email_address: req.params.email,
      status: 'subscribed',
    },
  }

  axios
    .request(options)
    .then(response => {
      res.status(200).send(response.data)
    })
    .catch(error => {
      console.error(error)
      res.status(200).json({
        error_message: error.message,
      })
    })
})
app.get(
  '/leadform/:name/:email/:number/:country/:country_code/:age',
  (req, res) => {
    // MailChimp API integration
    // https://mailchimp.com/developer/marketing/api/list-members/add-member-to-list/

    const API_KEY = 'a11273356df50d0f17aa282a790f881a-us20',
      API_DC = 'us20',
      LISTID = '402717cbe1'

    var options = {
      method: 'POST',
      url:
        'https://' +
        API_DC +
        '.api.mailchimp.com/3.0/lists/' +
        LISTID +
        '/members',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + API_KEY,
      },
      data: {
        merge_fields: {
          MMERGE5: req.params.number,
          MMERGE2: req.params.country,
          FNAME: req.params.name,
          MMERGE8: req.params.country_code,
          MMERGE6: req.params.age,
        },
        email_address: req.params.email,
        status: 'subscribed',
      },
    }

    axios
      .request(options)
      .then(response => {
        res.status(200).send(response.data)
      })
      .catch(error => {
        console.error(error)
        res.status(200).json({
          error_message: error.message,
        })
      })
  }
)
app.get('/:countryiso?/facebook_catalog_rss.xml', (req, res) => {
  const locale = req.params.countryiso ? req.params.countryiso : 'en'
  const currencySymbols = {
    in: 'INR',
    ca: 'CAD',
    it: 'GBP',
    es: 'GBP',
    de: 'EUR',
    fr: 'EUR',
    gb: 'GBP',
    en: 'USD',
  }
  var options = {
    method: 'GET',
    url: `https://7601ca579ffd169dac4e4fd132d9779e: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@${
      process.env.STORE_URL_SECONDARY
    }/admin/api/2022-01/products/6628829003838.json?presentment_currencies=${
      currencySymbols.hasOwnProperty(locale) ? currencySymbols[locale] : 'USD'
    }`,
    headers: { 'X-Shopify-Api-Features': 'include-presentment-prices' },
  }

  var optionsMax = {
    method: 'GET',
    url: `https://7601ca579ffd169dac4e4fd132d9779e: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@${
      process.env.STORE_URL_SECONDARY
    }/admin/api/2022-01/products/6617542721598.json?presentment_currencies=${
      currencySymbols.hasOwnProperty(locale) ? currencySymbols[locale] : 'USD'
    }`,
    headers: { 'X-Shopify-Api-Features': 'include-presentment-prices' },
  }

  axios
    .all([axios.request(options), axios.request(optionsMax)])
    .then(
      axios.spread((response, responseMax) => {
        let lang =
          req.params.countryiso !== undefined
            ? '/' + req.params.countryiso + '/'
            : '/'
        var responseXML = ''
        responseXML +=
          '<?xml version="1.0" encoding="UTF-8"?><rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">'
        responseXML += '<channel>'
        responseXML +=
          '<title>Miko 3 - Personal AI Robot For Kids | Miko Advanced STEM toys</title>'
        responseXML +=
          `<link>https://${process.env.STORE_PRIMARY_URL}` + lang + '</link>'
        responseXML +=
          '<description>Miko 3 is an advanced AI robot toy made to engage, educate and entertain kids. Miko 3 robot is a programmable and voice activated STEM toy to make kids smarter.</description>'

        // Miko 3
        for (let [i, variant] of response.data.product.variants.entries()) {
          responseXML += '<item>'
          responseXML += '<g:id>' + variant.id + '</g:id>'
          responseXML +=
            '<g:title><![CDATA[' + response.data.product.title + ']]></g:title>'
          responseXML +=
            '<g:description><![CDATA[' +
            response.data.product.body_html.replace(/<\/?[^>]+>/gi, ' ') +
            ']]></g:description>'
          responseXML +=
            `<g:link>https://${process.env.STORE_PRIMARY_URL}` +
            lang +
            'store/miko3?Color=' +
            encodeURIComponent(variant.title) +
            '</g:link>'
          responseXML +=
            '<g:image_link>' +
            response.data.product.images
              .find(img => img.id === variant.image_id)
              .src.split('?')[0] +
            '</g:image_link>'
          responseXML += '<g:condition>new</g:condition>'
          responseXML += '<g:availability>' + 'in stock' + '</g:availability>'
          responseXML +=
            '<g:price>' +
            variant.presentment_prices[0].price.amount +
            ' ' +
            currencySymbols[locale] +
            '</g:price>' // NEED TO CHANGE CURRENCY AND PRICE AS PER LOCALE
          responseXML += '<g:brand>Miko</g:brand>'
          responseXML += '<g:color>' + variant.title + '</g:color>'
          responseXML +=
            '<g:item_group_id>' +
            response.data.product.id +
            '</g:item_group_id>'
          responseXML += '</item>'
        }
        // Miko Max
        for (let [i, variant] of responseMax.data.product.variants.entries()) {
          responseXML += '<item>'
          responseXML += '<g:id>' + variant.id + '</g:id>'
          responseXML +=
            '<g:title><![CDATA[' +
            responseMax.data.product.title +
            ']]></g:title>'
          responseXML +=
            '<g:description><![CDATA[' +
            responseMax.data.product.body_html.replace(/<\/?[^>]+>/gi, ' ') +
            ']]></g:description>'
          responseXML +=
            `<g:link>https://${process.env.STORE_PRIMARY_URL}` +
            lang +
            'store/mikomax</g:link>'
          responseXML +=
            '<g:image_link>' +
            responseMax.data.product.images
              .find(img => img.id === variant.image_id)
              .src.split('?')[0] +
            '</g:image_link>'
          responseXML += '<g:condition>new</g:condition>'
          responseXML += '<g:availability>' + 'in stock' + '</g:availability>'
          responseXML +=
            '<g:price>' +
            variant.presentment_prices[0].price.amount +
            ' ' +
            currencySymbols[locale] +
            '</g:price>' // NEED TO CHANGE CURRENCY AND PRICE AS PER LOCALE
          responseXML += '<g:brand>Miko</g:brand>'
          responseXML +=
            '<g:item_group_id>' +
            responseMax.data.product.id +
            '</g:item_group_id>'
          responseXML += '</item>'
        }

        responseXML += '</channel>'
        responseXML += '</rss>'

        res.status(200).set('Content-Type', 'text/xml').send(responseXML)
      })
    )
    .catch(error => {
      console.error(error)
      let lang =
        req.params.countryiso !== 'us' ? '/' + req.params.countryiso + '/' : '/'
      var responseXML = ''
      responseXML +=
        '<?xml version="1.0" encoding="UTF-8"?><rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">'
      responseXML += '<channel>'
      responseXML +=
        '<title>Miko 3 - Personal AI Robot For Kids | Miko Advanced STEM toys</title>'
      responseXML +=
        `<link>https://${process.env.STORE_PRIMARY_URL}` + lang + '</link>'
      responseXML +=
        '<description>Miko 3 is an advanced AI robot toy made to engage, educate and entertain kids. Miko 3 robot is a programmable and voice activated STEM toy to make kids smarter.</description>'
      responseXML += '</channel>'
      responseXML += '</rss>'

      res.status(200).set('Content-Type', 'text/xml').send(responseXML)
    })
})

app.get('/trackorder/:order_name/:email', (req, res) => {
  if (req.params.order_name === undefined || req.params.order_name === '') {
    res.status(200).json({
      status: 400,
      message: 'Order Number and Email are required fields',
    })
  } else if (req.params.email === undefined || req.params.email === '') {
    res.status(200).json({
      status: 400,
      message: 'Order Number and Email are required fields',
    })
  }
  const shopifyAPIKey = '7601ca579ffd169dac4e4fd132d9779e',
    shopifyPass = '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',
    shopDomain = 'mikoin.myshopify.com',
    shopifyAPIVer = 'admin/api/2021-10'
  var options = {
    method: 'GET',
    url:
      'https://' +
      shopifyAPIKey +
      ':' +
      shopifyPass +
      '@' +
      shopDomain +
      '/' +
      shopifyAPIVer +
      '/orders.json?name=' +
      req.params.order_name +
      '&status=any',
  }
  console.log(options)
  axios
    .request(options)
    .then(response => {
      if (
        response.data.orders === undefined ||
        response.data.orders.length == 0
      ) {
        res.status(200).json({
          status: 400,
          message: 'Please Enter a valid order name',
        })
      } else if (req.params.email != response.data.orders[0].customer.email) {
        res.status(200).json({
          status: 400,
          message:
            'Email not associated with given order number, Please try with correct email',
        })
      } else {
        let responseJson = {
          fulfillment_status:
            response.data.orders[0].fulfillment_status != null
              ? response.data.orders[0].fulfillment_status
              : null,
          tracking_company:
            response.data.orders[0].fulfillments.length > 0
              ? response.data.orders[0].fulfillments[0].tracking_company
              : null,
          tracking_number:
            response.data.orders[0].fulfillments.length > 0
              ? response.data.orders[0].fulfillments[0].tracking_number
              : null,
          tracking_numbers:
            response.data.orders[0].fulfillments.length > 0 &&
            response.data.orders[0].fulfillments[0].tracking_numbers !==
              undefined
              ? response.data.orders[0].fulfillments[0].tracking_numbers
              : null,
          tracking_url:
            response.data.orders[0].fulfillments.length > 0
              ? response.data.orders[0].fulfillments[0].tracking_url
              : null,
          tracking_urls:
            response.data.orders[0].fulfillments.length > 0 &&
            response.data.orders[0].fulfillments[0].tracking_urls !== undefined
              ? response.data.orders[0].fulfillments[0].tracking_urls
              : null,
          order_status_url: response.data.orders[0].order_status_url,
        }

        res.status(200).json({
          status: 200,
          data: responseJson,
        })
      }
    })
    .catch(error => {
      console.error(error)
      res.status(200).json({
        status: 400,
        message: 'Something went wrong, try again!',
        error_message: error.message,
      })
    })
})

// ChargeBee portal session
app.post('/cb-session', (req, res) => {
  if (req.body === undefined) {
    res.status(200).send({})
  }
  const currentUserEmail = req.body.email
  const cbDomain = 'pushq-test',
    cbToken = 'test_uUP1nf5ljoBMZxjn8iGcugxmJ0cdOw4aWo'
  const encryptToken = Buffer.from(cbToken).toString('base64')

  var options = {
    method: 'GET',
    url: 'https://' + cbDomain + '.chargebee.com/api/v2/customers',
    params: {
      'email[is]': currentUserEmail,
    },
    headers: {
      Authorization: 'Basic ' + encryptToken, // dGVzdF91VVAxbmY1bGpvQk1aeGpuOGlHY3VneG1KMGNkT3c0YVdvOg==
    },
  }

  axios
    .request(options)
    .then(function (response) {
      if (response.data.list.length <= 0) res.status(200).send({})
      else {
        var options = {
          method: 'POST',
          url: 'https://' + cbDomain + '.chargebee.com/api/v2/portal_sessions',
          params: {
            'customer[id]': response.data.list[0].customer.id,
            return_url: 'https://google.com/',
          },
          headers: {
            Authorization: 'Basic ' + encryptToken, // dGVzdF91VVAxbmY1bGpvQk1aeGpuOGlHY3VneG1KMGNkT3c0YVdvOg==
          },
        }

        axios
          .request(options)
          .then(function (responseData) {
            res.send(responseData.data.portal_session)
          })
          .catch(function (error) {
            console.error(error)
            res.send({})
          })
      }
    })
    .catch(function (error) {
      console.error(error)
      res.send({})
    })
})
